(function () {
  const canvas = document.getElementById("canvas1");
  const ctx = canvas.getContext("2d");
  canvas.width = 800;
  canvas.height = 500;

  let score = 0;
  let gameFrame = 0;
  ctx.font = "bold 48px serif";

  const mouse = {
    x: canvas.width / 2,
    y: canvas.height / 2,
    click: false,
  };
  let canvasPostion = canvas.getBoundingClientRect();
  canvas.addEventListener("mousedown", function (e) {
    mouse.x = e.x - canvasPostion.left;
    mouse.y = e.y - canvasPostion.top;
    mouse.click = true;
    console.log(
      mouse.x,
      mouse.y,
      e.x,
      e.y,
      canvasPostion.left,
      canvasPostion.top,
      canvasPostion
    );
  });

  canvas.addEventListener("mouseup", function () {
    mouse.click = false;
  });

  class Player {
    constructor() {
      this.x = canvas.width;
      this.y = canvas.height / 2;
      this.radius = 50;
    }

    update() {
      const dx = this.x - mouse.x;
      const dy = this.y - mouse.y;
      if (mouse.x != this.x) {
        this.x -= dx / 25;
      }
      if (mouse.y != this.y) {
        this.y -= dy / 25;
      }
    }

    draw() {
      if (mouse.click) {
        ctx.lineWidth = 0.2;
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
      }
      ctx.fillStyle = "#FFA500";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
      ctx.fill();
      ctx.closePath();
    }
  }

  const player = new Player();

  const bubbleArray = [];
  class Bubble {
    constructor() {
      this.x = Math.random() * canvas.width;
      this.y = canvas.height;
      this.radius = 50;
      this.speed = Math.random() * 5 + 1;
      this.distance;
      this.count = false;
    }

    upadte() {
      this.y -= this.speed;
      const dx = this.x - player.x;
      const dy = this.y - player.y;
      this.distance = Math.sqrt(dx * dx + dy * dy);
    }
    draw() {
      ctx.fillStyle = "blue";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
      ctx.fill();
      ctx.closePath();
      ctx.stroke();
    }
  }

  function handleBubble() {
    if (gameFrame % 50 == 0) {
      bubbleArray.push(new Bubble());
    }
    for (let i = 0; i < bubbleArray.length; i++) {
      bubbleArray[i].upadte();
      bubbleArray[i].draw();
    }
    for (let i = 0; i < bubbleArray.length; i++) {
      if (bubbleArray[i].y < 0 - bubbleArray[i].radius * 2) {
        bubbleArray.splice(i, 1);
      }
      if (bubbleArray[i].distance < bubbleArray[i].radius + player.radius) {
        if (!bubbleArray[i].count) {
          score++;
          bubbleArray[i].count = true;
          bubbleArray.splice(i, 1);
        }
      }
    }
  }

  function animate() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    handleBubble();
    player.update();
    player.draw();
    ctx.fillStyle = "black";
    ctx.fillText("score: " + score, 10, 50);
    gameFrame++;
    requestAnimationFrame(animate);
  }
  animate();
})();
